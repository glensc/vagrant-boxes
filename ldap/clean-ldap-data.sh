#!/bin/sh
set -xe
rm -fvr /var/lib/openldap-data/{__db.*,*.bdb,log.*,alock}
