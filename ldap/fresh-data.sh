#!/bin/sh
set -xe
service ldap stop
> /var/log/messages
rm -fvr /var/lib/openldap-data/{__db.*,*.bdb,log.*,alock}
service ldap start
