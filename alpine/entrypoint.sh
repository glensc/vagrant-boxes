#!/bin/sh
set -xeu

ssh-keygen -A
exec /usr/sbin/sshd -D
