# -*- mode: ruby -*-
# vi: set ft=ruby :

hostname = File.basename(File.dirname(__FILE__))
user = ENV['USER'] || 'vagrant'

print "\033k#{hostname}\033\\" if ENV['TERM'][/screen/]

Vagrant.configure(2) do |config|
	config.vm.box = 'pld32'
	config.vm.box_url = "ftp://ftp.pld-linux.org/people/glen/vm/#{config.vm.box}.box"

	config.ssh.forward_agent = true
	config.vm.hostname = hostname

	# fight against port collision chance: use crc32 of hostname as base port
	require 'zlib'
	port = 2100 + Zlib::crc32(user + hostname) % 4000
	config.vm.usable_port_range = (port..port + 100)

	# make rebuilds faster to preserve cache outside vm
	if Vagrant.has_plugin?('vagrant-cachier')
		config.cache.enable :generic, {
			'poldek' => { cache_dir: '/var/cache/poldek' },
		}
		config.cache.scope = :box
		config.cache.auto_detect = false
	else
		Dir.mkdir('cache') unless Dir.exists?('cache')
		config.vm.synced_folder 'cache', '/var/cache/poldek', owner: 'vagrant', group: 'vagrant'
	end

	# enable these two when running in lumpy
	config.vm.hostname += '.delfi.lan'
	# enable if need host to be accessible from lan
#	config.vm.network :public_network, { bridge: 'eth1', auto_config: true }

	# http://docs.vagrantup.com/v2/provisioning/shell.html
	config.vm.provision :shell, :path => "../bootstrap.sh", :args => user

#	config.vm.provision :shell, :inline => %q[
#		poldek -u chef --noask
#	]

	config.vm.provision :chef_client do |chef|
		chef.version = '11.16.4'
		chef.chef_server_url = "https://chef-server.delfi.lan"
		chef.validation_key_path = "chef-validator.pem"
		chef.validation_client_name = "chef-validator"
		chef.log_level = :info
		chef.verbose_logging = true
		chef.file_cache_path = "/var/cache/chef"
		chef.file_backup_path = "/var/lib/chef/backup"
		chef.environment = "test"

		chef.add_role 'base_pld'

		chef.json = {
			:openldap => {
				:server => 'ldap',
				:server_uri => 'ldap://ldap/ ldap://ldap2/',
			},
			:country => 'ee',
			:ntp => {
				:servers => %w[212.47.200.1 212.47.200.2 212.47.200.5 212.47.200.13],
			},
		}
	end
end
