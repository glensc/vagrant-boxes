#!/bin/sh
set -e

update_box() {
	local box=$1 url=$2
	vagrant box remove $box || :
	vagrant box add $box "$url"
}


if [ "$1" ]; then
	update_box "$@"
	exit $?
fi

update_box pld64 http://jenkins.delfi.net/pld64.box
update_box pld32 http://jenkins.delfi.net/pld32.box
