#!/bin/sh
vagrant destroy -f
knife node delete -y $(basename $(pwd)).delfi.lan
knife client delete -y $(basename $(pwd)).delfi.lan
time vagrant up
