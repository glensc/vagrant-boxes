#!/bin/sh

#arch=$(rpm -E %{_arch}) # gives i386 without rpm-build installed
arch=$(rpm -q rpm --qf '%{ARCH}')
dist=$(rpm -E %{pld_release})

need_poldek_up=true
install() {
	local pkg=$1; shift

	rpm -q $pkg && return
	$need_poldek_up && { poldek --up && need_poldek_up=false; }
	poldek $pkg -u "$@"
}

install diffutils -s http://distrib.dev.delfi.ee/pld/dists/$dist/PLD/$arch/RPMS/
install poldek-delfi -s http://distrib.dev.delfi.ee/delfi/pld/$dist/$arch/

# reset need_poldek_up, as it was cached wrong for specified source name only
need_poldek_up=true

install wget vim-syntax-spec glibc-localedb-delfi bash-completion gawk iputils-ping bind-utils xfsprogs xz

test -e /etc/poldek/repos.d/carme.conf || wget -c http://carme.pld-linux.org/~glen/$dist/$arch/carme.conf -O /etc/poldek/repos.d/carme.conf

rpm -q VirtualBox-guest || {
	poldek -u VirtualBox-guest --noask --force
	service vboxservice start
}

if ! grep -q SUDO_USER ~vagrant/.bash_profile && test -n "$1"; then
   	cat <<-EOF >> ~vagrant/.bash_profile
	# be me
	[ "\$SUDO_USER" = "vagrant" ] && SUDO_USER=$1
	EOF
	shift
fi
